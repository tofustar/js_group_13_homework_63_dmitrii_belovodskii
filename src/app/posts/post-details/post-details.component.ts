import { Component, OnInit } from '@angular/core';
import { Post } from '../../shared/post.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {

  post: Post | null = null;
  postId = '';

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.postId = params['id'];
      this.http.get<Post>(`https://js-group-13-default-rtdb.firebaseio.com/posts/`+ this.postId + `.json`)
        .subscribe(post => {
          this.post = post;
        });
    });
  }

  deletePost() {
    this.http.delete<Post>(`https://js-group-13-default-rtdb.firebaseio.com/posts/`+ this.postId + `.json`).subscribe();
    void this.router.navigate(['/posts']);
  }

}
