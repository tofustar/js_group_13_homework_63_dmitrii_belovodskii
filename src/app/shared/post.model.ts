export class Post {
  constructor(
    public id: string,
    public dateOfCreate: string,
    public title: string,
    public description: string,
  ) {}
}
