import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params } from '@angular/router';
import { Post } from '../shared/post.model';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {

  title = '';
  description = '';
  postId: number | null = null;

  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit(): void {}

  addPost() {
    const title = this.title;
    const description = this.description;
    this.route.params.subscribe((params: Params) => {
      if (params['id']) {
        this.postId = params['id'];
        const body = {title, description};
        this.http.patch<Post>(`https://js-group-13-default-rtdb.firebaseio.com/posts/`+ this.postId + `.json`, body)
          .subscribe();
      } else {
        const dateOfCreate = new Date();
        const body = {dateOfCreate, description, title };
        this.http.post('https://js-group-13-default-rtdb.firebaseio.com/posts.json', body).subscribe();
      }
    })
  }
}
