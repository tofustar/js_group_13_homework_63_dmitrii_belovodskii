import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: '<h1>Something went wrong</h1>'
})

export class NotFoundComponent {}
